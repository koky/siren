@extends('layouts.app')

@section('content')
	<div class="container">
		<h1>Gestion des contacts</h1>
		<form>
		  <div class="form-group">
		    <label for="exampleInputEmail1">N° Sirene</label>
		    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="N° Sirene">
		 
		  </div>
		  <div class="form-group">
		    <label for="exampleInputEmail1">Nom socièté</label>
		    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nom socièté">

		  </div>
		  <div class="form-group">
		    <label for="exampleInputEmail1">Adresse complète</label>
		    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Adresse complète">

		  </div>
		  <div class="form-group">
		    <label for="exampleInputEmail1">Chiffre d'affaire</label>
		    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Chiffre d'affaire">

		  </div>
		  <div class="form-group">
		    <label for="exampleInputEmail1">Code NAF</label>
		    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Code NAF">

		  </div>
		  <div class="form-group">
		    <label for="exampleInputEmail1">Email address</label>
		    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Email address">
		  </div>
		   <button type="submit" class="btn btn-primary">Submit</button>
		   <a href="/tableau" class="btn btn-primary btn-lg active" role="button">Annuler</a>
		</form>
	</div>
@endsection