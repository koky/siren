<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEtablissementsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('etablissements', function(Blueprint $table)
		{
			$table->string('sirene', 9)->index('sirene');
			$table->string('nic', 5);
			$table->string('l1_normalisee', 38)->nullable();
			$table->string('l2_normalisee', 38)->nullable();
			$table->string('l3_normalisee', 38)->nullable();
			$table->string('l4_normalisee', 38)->nullable();
			$table->string('l5_normalisee', 38)->nullable();
			$table->string('l6_normalisee', 38)->nullable();
			$table->string('l7_normalisee', 38)->nullable();
			$table->string('l1_declaree', 38)->nullable();
			$table->string('l2_declaree', 38)->nullable();
			$table->string('l3_declaree', 38)->nullable();
			$table->string('l4_declaree', 38)->nullable();
			$table->string('l5_declaree', 38)->nullable();
			$table->string('l6_declaree', 38)->nullable();
			$table->string('l7_declaree', 38)->nullable();
			$table->string('numvoie', 4)->nullable();
			$table->string('indrep', 1)->nullable();
			$table->string('typvoie', 4)->nullable();
			$table->string('libvoie', 32)->nullable();
			$table->string('codpos', 5)->nullable();
			$table->string('cedex', 5)->nullable();
			$table->string('rpet', 2)->nullable();
			$table->string('libreg', 70)->nullable();
			$table->string('depet', 2)->nullable();
			$table->string('arronet', 2)->nullable();
			$table->string('ctonet', 3)->nullable();
			$table->string('comet', 3)->nullable();
			$table->string('libcom', 32)->nullable();
			$table->string('du', 2)->nullable();
			$table->string('tu', 1)->nullable();
			$table->string('uu', 2)->nullable();
			$table->string('epci', 9)->nullable();
			$table->string('tcd', 2)->nullable();
			$table->string('zemet', 4)->nullable();
			$table->string('siege', 1)->nullable();
			$table->string('enseigne', 140)->nullable();
			$table->string('ind_publipo', 1)->nullable();
			$table->string('diffcom', 1)->nullable();
			$table->string('amintret', 6)->nullable();
			$table->string('natetab', 1)->nullable()->index('natetab');
			$table->string('apet700', 5)->nullable()->index('apet700_2');
			$table->string('dapet', 4)->nullable();
			$table->string('tefet', 2)->nullable()->index('tefet');
			$table->string('efetcent', 6)->nullable();
			$table->string('defet', 4)->nullable();
			$table->string('origine', 2)->nullable();
			$table->string('dcret', 8)->nullable();
			$table->string('date_deb_etat_adm_et', 8)->nullable();
			$table->string('activnat', 2)->nullable();
			$table->string('lieuact', 2)->nullable();
			$table->string('actisurf', 2)->nullable();
			$table->string('saisonat', 2)->nullable();
			$table->string('modet', 1)->nullable();
			$table->string('prodet', 1)->nullable();
			$table->string('prodpart', 1)->nullable();
			$table->string('auxilt', 1)->nullable();
			$table->primary(['sirene','nic']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('etablissements');
	}

}
