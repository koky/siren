<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSocietesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('societes', function(Blueprint $table)
		{
			$table->foreign('nj', 'societes_ibfk_1')->references('nj')->on('nature')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('apen700', 'societes_ibfk_2')->references('ape')->on('ape')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('tefen', 'societes_ibfk_3')->references('efet')->on('effectifs')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('societes', function(Blueprint $table)
		{
			$table->dropForeign('societes_ibfk_1');
			$table->dropForeign('societes_ibfk_2');
			$table->dropForeign('societes_ibfk_3');
		});
	}

}
