<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToNatetabTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('natetab', function(Blueprint $table)
		{
			$table->foreign('natetab', 'natetab_ibfk_1')->references('natetab')->on('etablissements')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('natetab', function(Blueprint $table)
		{
			$table->dropForeign('natetab_ibfk_1');
		});
	}

}
