<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSocietesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('societes', function(Blueprint $table)
		{
			$table->string('sirene', 9)->primary();
			$table->string('nomen_long', 131)->nullable();
			$table->string('sigle', 20)->nullable();
			$table->string('nom', 100)->nullable();
			$table->string('prenom', 30)->nullable();
			$table->string('civilite', 1)->nullable();
			$table->string('rna', 10)->nullable();
			$table->string('nicsiege', 5)->nullable();
			$table->string('rpen', 2)->nullable();
			$table->string('depcomen', 5)->nullable();
			$table->string('adr_mail', 80)->nullable();
			$table->string('nj', 4)->nullable()->index('nj');
			$table->string('apen700', 5)->nullable()->index('apen700');
			$table->string('dapen', 4)->nullable();
			$table->string('aprm', 6)->nullable();
			$table->string('essen', 1)->nullable();
			$table->string('dateess', 8)->nullable();
			$table->string('tefen', 2)->nullable()->index('tefen');
			$table->string('efencent', 6)->nullable();
			$table->string('defen', 4)->nullable();
			$table->string('categorie', 5)->nullable();
			$table->string('dcren', 8)->nullable();
			$table->string('amintren', 6)->nullable();
			$table->string('monoact', 1)->nullable();
			$table->string('moden', 1)->nullable();
			$table->string('proden', 1)->nullable();
			$table->char('esaann', 4)->nullable();
			$table->string('tca', 1)->nullable();
			$table->string('esaapen', 5)->nullable();
			$table->string('esasec1n', 5)->nullable();
			$table->string('esasec2n', 5)->nullable();
			$table->string('esasec3n', 5)->nullable();
			$table->string('esasec4n', 5)->nullable();
			$table->date('datemaj')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('societes');
	}

}
